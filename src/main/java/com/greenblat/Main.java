package com.greenblat;

public class Main {
    public static void main(String[] args) {

        int countNumber = 11;

        int last = 1;
        int curr = 1;

        System.out.print(last + " " + curr + " ");

        int i = 2;
        while (i++ < countNumber) {
            int tmp = curr;
            curr += last;
            last = tmp;

            System.out.print(curr + " ");
        }

        System.out.println();

        last = 1;
        curr = 1;
        System.out.print(last + " " + curr + " ");

        for (i = 2; i < countNumber; i++) {
            int tmp = curr;
            curr += last;
            last = tmp;

            System.out.print(curr + " ");
        }
    }
}